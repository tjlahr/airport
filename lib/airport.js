var Airport = function(info){
    this.info = info;

    this.planes = [];

    this.departures = {
        status: 'normal'
    };

    this.arrivals = {
        status: 'normal'
    };
};

module.exports = Airport;
