var assert = require('chai').assert,
    Airplane = require('../lib/airplane');

describe('Airplane', function(){
    var airplane = new Airplane({
        make: 'Boeing',
        model: '747',
        tailNumber: 'Z12345'
    });

    it('should be a constructor', function(){
        assert.isFunction(Airplane);
    });

    it('has a tail number which identifies it uniquely', function(){
        assert.equal(airplane.info.tailNumber, 'Z12345');
    });

    it('has make and model properties', function(){
        assert.equal(airplane.info.make, 'Boeing');
        assert.equal(airplane.info.model, '747');
    });

    it("keeps track of how many miles it's flown", function(){
        assert.isNumber(airplane.milesFlown);
    });

    it("can take off", function(){
        assert.isFunction(airplane.takeOff); 
    });

    it("can land", function(){
        assert.isFunction(airplane.land); 
    });
});
