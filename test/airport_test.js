var assert = require('chai').assert,
Airport = require('../lib/airport');

console.log('test');

describe("Airport", function(){

    var airport = new Airport({
        name: 'Washington Dullas',
        code: 'IAD',
        coordinates: [38.944444, -77.455833],
        elevation: '313 ft',
        international: true
    });

    it("should be JavaScript contructor", function(){
        assert.isFunction(Airport);
    });

    it("should have planes", function(){
        assert.isDefined(airport.planes);
    });

    describe('info', function(){
        var info = airport.info;

        it("it should return info about itself", function(){
            assert.typeOf(airport.info, 'object');
            assert.equal(airport.info.name, 'Washington Dullas');
            assert.deepEqual(airport.info.coordinates, [38.944444, -77.455833]);
            assert.deepEqual(airport.info.code, 'IAD');
            assert.equal(airport.info.elevation, '313 ft');
            assert.isBoolean(airport.info.international);
        });
    });

    describe('Departures', function(){
        it("should be an object on airport", function(){
            assert.isObject(airport.departures);
        });

        it("should report its status", function(){
            assert.equal(airport.departures.status, "normal");
        });
    });

    describe('Arrivals', function(){
        it('should be an object on airport', function(){
            assert.isObject(airport.arrivals);
        });

        it("should report its status", function(){
            assert.equal(airport.arrivals.status, 'normal');
        });
    });
});
