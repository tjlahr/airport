function Airplane(info){
    this.info = info;
    this.milesFlown = 5000;
};

Airplane.prototype.takeOff = function(){
    console.log('taking off');
}

Airplane.prototype.land = function(){
    console.log('landing');
}

module.exports = Airplane;
